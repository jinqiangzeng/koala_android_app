package com.koala.timemaster;

import java.util.Calendar;

/**
 * Created by jq.zeng on 8/17/15.
 */
public interface ChangeCalendarView {
    /**
     * switch Calendar Fragment view between month view and day view
     * @param fragmentToShow
     */
    void changeCalendarView(CalendarContainerFragment.FragmentToShow fragmentToShow);

    /**
     * set day to display on day view
     * @param day
     */
    void setDisplayDate(Calendar day);

    /**
     * get current display date of day view
     * @return
     */
    Calendar getDisplayDate();
}
