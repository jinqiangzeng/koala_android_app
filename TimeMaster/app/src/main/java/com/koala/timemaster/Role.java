package com.koala.timemaster;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.InputMismatchException;

/**
 * Created by jq.zeng on 8/16/15.
 */
public class Role implements Parcelable {
    private String mTag;
    private int weight;
    private boolean isChecked;

    public Role(String tag, int weight, boolean isChecked) {
        mTag = tag;
        this.weight = weight;
        this.isChecked = isChecked;
    }

    public Role() {
        mTag = "";
        weight = 3;
    }

    public String getTag() {
        return mTag;
    }

    public int getWeight() {
        return weight;
    }

    public void setTag(String tag) {
        mTag = tag;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isChecked() {return isChecked; }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String toString() {
        return mTag;
    }

    public String saveFormatString() {
        return mTag + "\n" + Integer.toString(weight);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(mTag);
        dest.writeInt(weight);
    }

    public static final Parcelable.Creator<Role> CREATOR
            = new Parcelable.Creator<Role>(){
        public Role createFromParcel(Parcel in) {
            return new Role(in);
        }

        public Role[] newArray(int size) {
            return new Role[size];
        }
    };

    /**
     * construct class Role from Parcel object
     * @param in
     */
    private Role(Parcel in) {
        mTag = in.readString();
        weight = in.readInt();
    }
}
