package com.koala.timemaster;

import com.google.android.gms.wallet.LineItem;

import java.util.List;

/**
 * Created by jq.zeng on 8/17/15.
 */
public interface TagOps {
    /**
     * load tags from file "tags.txt"
     * @return
     */
    List<Role> loadTagsFromFile();

    /**
     * save tags to file "tags.txt"
     */
     void saveTagsToFile(List<Role> tags);

    /**
     * load tags from memory, used to communicate between MoreTagsNewActivity and TagNewDialogFragment
     * @return
     */
    List<Role> loadTags();


    /**
     * add tag to list, used to communicate between MoreTagsNewActivity and TagNewDialogFragment
     * @param role
     * @return
     */
    List<Role> addTag(Role role);
}
