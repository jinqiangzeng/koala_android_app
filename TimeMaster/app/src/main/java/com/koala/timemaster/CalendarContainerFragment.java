package com.koala.timemaster;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jqzeng.mylibrary.event.EventModel;

import java.util.Calendar;
import java.util.List;

/**
 * Created by jq.zeng on 7/19/15.
 */
public class CalendarContainerFragment extends BaseFragment {
    private final String TAG = "calendarContainer";


    private static FragmentToShow mFragmentToShow = FragmentToShow.None;
    private CalendarFragment mMonthFragment;
    private CalendarWeekFragment mWeekFragment;

    public enum FragmentToShow{
        None,Month,Week
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        if (mFragmentToShow == FragmentToShow.None){
            mFragmentToShow = FragmentToShow.Week;
        }

        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

        switch(mFragmentToShow) {
            case None:
            case Month:
                mMonthFragment = new CalendarFragment();
                transaction.add(R.id.calendar_container, mMonthFragment, "CalendarFragment");
                break;
            case Week:
                mWeekFragment = new CalendarWeekFragment();
                transaction.add(R.id.calendar_container, mWeekFragment, "WeekFragment");
                break;
        }

        transaction.addToBackStack(null);
        transaction.commit();
        return inflater.inflate(R.layout.calendar, container, false);
    }

    public void setFragmentToShow(FragmentToShow fragmentToShow) {
        this.mFragmentToShow = fragmentToShow;
        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

        switch(mFragmentToShow) {
            case None:
            case Month:
                mMonthFragment = new CalendarFragment();
                transaction.replace(R.id.calendar_container, mMonthFragment, "CalendarFragment");
                break;
            case Week:
                mWeekFragment = new CalendarWeekFragment();
                transaction.replace(R.id.calendar_container, mWeekFragment, "WeekFragment");
                break;
        }

        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "View Created");
    }

}