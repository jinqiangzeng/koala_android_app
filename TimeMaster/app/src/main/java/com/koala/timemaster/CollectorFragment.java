package com.koala.timemaster;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.koushikdutta.async.util.Charsets;
import com.melnykov.fab.FloatingActionButton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by jq.zeng on 6/21/15.
 */
public class CollectorFragment extends BaseFragment {

    private final String TAG = "CollectorFragment";
    public static final String FILE_NAME = "CollectorEventS1.json";

    private ExpandableListView mExpandableListView;
    private TextView mTextView;

    private String currentDisplay = "All";
    private Role mRole;
    private List<CollectorEvent> listCollectorEvents;    //save all CollectorEvents
    private HashMap<String,List<CollectorEvent>> listRoleEvents;  //save CollectorEvents according to different role.
    private MyExpandableListAdapter mAdapter;

    private EventOps mCallback;

    public final static  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            mCallback = (EventOps) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CalendarContainerFragment.ChangeCalendarView");
        }
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreate");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.collector, container, false);

        //load CollectorEvents from file
        if (listCollectorEvents == null){
            listCollectorEvents = loadCollectorEvents();
        }
        //construct listRoleEvents according to Role
        if(listRoleEvents == null) {
            listRoleEvents = new HashMap<String, List<CollectorEvent>>();
            for (CollectorEvent collectorEvent : listCollectorEvents){
                if(listRoleEvents.get(collectorEvent.getRole()) == null)
                    listRoleEvents.put(collectorEvent.getRole(), new ArrayList<CollectorEvent>());
                listRoleEvents.get(collectorEvent.getRole()).add(collectorEvent);
            }
        }

        mExpandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);

        TagOps tagOps = new TagFileOps(view.getContext());
        List<Role> tags = tagOps.loadTagsFromFile();
        List<String> spinnerContent = new ArrayList<String>();
        //add All, and roles to spinner in sequence
        spinnerContent.add("All");
        for (int i= 0; i < tags.size(); i++) {
            spinnerContent.add(tags.get(i).getTag());
        }

        //construct spinner to display all tags and "All", filter the events by tags or display all events
        Spinner mSpinner = (Spinner) view.findViewById(R.id.collector_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(),android.R.layout.simple_spinner_dropdown_item,spinnerContent);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, parent.getSelectedItem().toString());
                currentDisplay = (String) parent.getSelectedItem();
                //refresh display list
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.attachToListView(mExpandableListView);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CollectorEventNewActivity.class);
                startActivityForResult(intent,1);
            }
        });

        Resources res = this.getResources();
        //Drawable devider = res.getDrawable(R.drawable.line);

        // Set ExpandableListView values
        mExpandableListView.setGroupIndicator(null);
        //    mExpandableListView.setDivider(devider);
        //   mExpandableListView.setChildDivider(devider);
        mExpandableListView.setDividerHeight(1);
        mExpandableListView.setClickable(true);
        // registerForContextMenu(mExpandableListView);

        //Creating static data in arraylist

        // Adding ArrayList data to ExpandableListView values

        if (mAdapter == null) {
            //Create ExpandableListAdapter Object
            mAdapter = new MyExpandableListAdapter();
            Log.i(TAG, "create new expandablelistAdapter");
        } else {
            // Refresh ExpandableListView data
            ((MyExpandableListAdapter) getExpandableListAdapter()).notifyDataSetChanged();
        }
        setListAdapter(mAdapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.i(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.i(TAG, "onPuase");
        //TODO save event to a file
        saveCollectorEvents(listCollectorEvents);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (listCollectorEvents.size() == 0){
            listCollectorEvents = loadCollectorEvents();
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * add CollectorEvent to listRoleEvents according to collectorEvent's role.
     * Add to listCollectorEvnets for "All" displaying at the same time.
     * @param collectorEvent  to be added CollectorEvent
     */
    public void addRoleEvent(CollectorEvent collectorEvent) {
        if (collectorEvent != null) {
            listCollectorEvents.add(collectorEvent);
            if(listRoleEvents.get(collectorEvent.getRole()) == null)
                listRoleEvents.put(collectorEvent.getRole(), new ArrayList<CollectorEvent>());
            listRoleEvents.get(collectorEvent.getRole()).add(collectorEvent);
        }
    }

    /**
     * get events according to role.
     * return listCollectorEvents if Role is "All"
     * @param filter
     *            role of the events.
     * @return List of CollectorEvents.
     */
    public List<CollectorEvent> getRoleEvents(String filter) {
        if(filter.equals("All") || filter.isEmpty()) {
            return listCollectorEvents;
        }
        if(listRoleEvents.get(filter) != null) {
            return listRoleEvents.get(filter);
        }
        return new ArrayList<CollectorEvent>();
    }

    public void setListAdapter(MyExpandableListAdapter adapter) {
        boolean hadAdapter = mAdapter != null;
        mAdapter = adapter;
        if (mExpandableListView != null) {
            mExpandableListView.setAdapter(adapter);
        }
    }

    public MyExpandableListAdapter getExpandableListAdapter() {
        return mAdapter;
    }

    /**
     * A Custom adapter to create Parent view (Used grouprow.xml) and Child View((Used childrow.xml).
     */
    private class MyExpandableListAdapter extends BaseExpandableListAdapter {


        private LayoutInflater inflater;

        public MyExpandableListAdapter() {
            // Create Layout Inflator
            inflater = LayoutInflater.from(getActivity());
        }

        private class GroupViewHolder {
            TextView titleView;
            CheckBox checkBox;
        }

        private class ChildViewHolder{
            TextView titleView;
        }

        // This Function used to inflate parent rows view

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parentView) {
            final CollectorEvent collectorEvent = getRoleEvents(currentDisplay).get(groupPosition);
            final GroupViewHolder viewHolder;
            Log.d(TAG, "getGroupView " + groupPosition);

            // Inflate grouprow.xml file for collectorEvent rows
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.collector_events, parentView, false);
                viewHolder = new GroupViewHolder();
                viewHolder.titleView = (TextView)convertView.findViewById(R.id.event_title);
                viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (GroupViewHolder)convertView.getTag();
            }

            // Get grouprow.xml file elements and set values
            viewHolder.titleView.setText(collectorEvent.getTitle());

            viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    viewHolder.checkBox.setChecked(isChecked);
                    Toast.makeText(getActivity().getApplicationContext(),collectorEvent.getTitle() + (isChecked?"is checked":"is unchecked"), Toast.LENGTH_SHORT).show();
                }
            });


            return convertView;
        }


        // This Function used to inflate child rows view
        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parentView) {
            final CollectorEvent collectorEvent = getRoleEvents(currentDisplay).get(groupPosition);
            ChildViewHolder viewHolder;

            // Inflate childrow.xml file for collectorSubEvent rows
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.collector_event_sub, parentView, false);
                viewHolder = new ChildViewHolder();
                viewHolder.titleView = ((TextView) convertView.findViewById(R.id.subevent));

                convertView.setTag(viewHolder);
            } else{
                viewHolder = (ChildViewHolder) convertView.getTag();
            }

            // Get childrow.xml file elements and set values
            viewHolder.titleView.setText(collectorEvent.getStringList().get(childPosition));

            return convertView;
        }


        @Override
        public Object getChild(int groupPosition, int childPosition) {
            //Log.i("Childs", groupPosition+"=  getChild =="+childPosition);
            return getRoleEvents(currentDisplay).get(groupPosition).getStringList().get(childPosition);
        }

        //Call when child row clicked
        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return getRoleEvents(currentDisplay).get(groupPosition).getDisplayItems();
        }


        @Override
        public Object getGroup(int groupPosition) {
            return getRoleEvents(currentDisplay).get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return getRoleEvents(currentDisplay).size();
        }

        //Call when parent row clicked
        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public void notifyDataSetChanged() {
            // Refresh List rows
            super.notifyDataSetChanged();
        }

        @Override
        public boolean isEmpty() {
            return ((getRoleEvents(currentDisplay) == null) || getRoleEvents(currentDisplay).isEmpty());
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

    }

    /**
     * receive CollectorEvent from CollectorEventNewActivity and add to display list
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG,"Entered onActivityResult()");

        if (requestCode == 1) {
            if(resultCode == getActivity().RESULT_OK) {
                CollectorEvent event = new CollectorEvent(data);
                Log.i(TAG, event.getRole() + " " + event.getLocation() + " " + event.getPriority().toString());
                //listCollectorEvents.add(event);
                addRoleEvent(event);
                mAdapter.notifyDataSetChanged();
                saveCollectorEvents(listCollectorEvents);
                mCallback.addCalendarEvent(event);
            }
        }
    }

    /**
     * load Collector Events from JSON file
     * @return All CollectorEvents in list
     */
    private List<CollectorEvent> loadCollectorEvents() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileInputStream fis = getActivity().openFileInput(FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                Log.d(TAG, "string builder len = "+ stringBuilder.length());
            }
            bufferedReader.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String json = stringBuilder.toString();
        Log.d(TAG, "json length = " + json.length() + ":" + json);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(new TypeToken<List<CollectorEvent>>(){}.getType(), new CollectorEventDeserializer());
        Gson gson = gsonBuilder.create();
        List<CollectorEvent> list = gson.fromJson(json, new TypeToken<List<CollectorEvent>>(){}.getType());
        if (list != null) {
            return list;
        } else {
            return new ArrayList<>();
        }

    }

    /**
     * Save event to JSON file
     */
    private void saveCollectorEvents(List<CollectorEvent> list) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(new TypeToken<List<CollectorEvent>>(){}.getType(), new CollectorEventSerializer());
            Gson gson = gsonBuilder.create();
            String s = gson.toJson(list, new TypeToken<List<CollectorEvent>>(){}.getType());
            Log.d(TAG, "writing JSON length =" + s.length() + " json : " + s);
            FileOutputStream fileOutputStream = getActivity().openFileOutput(FILE_NAME,getActivity().MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Charsets.UTF_8.name());
            Writer jwriter = new BufferedWriter(outputStreamWriter);
            jwriter.write(s);

            jwriter.close();  //closing sequence mustn't be wrong
            outputStreamWriter.close();
            fileOutputStream.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * sort Collector events according to time. The 1st priority event stand 1st place.
     */
    public interface SortCollectorEvent{
        List<CollectorEvent> sortCollectorEvents(List<CollectorEvent> collectorEventList);
    }

}
