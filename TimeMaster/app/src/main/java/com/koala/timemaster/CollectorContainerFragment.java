package com.koala.timemaster;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jq.zeng on 7/15/15.
 */
public class CollectorContainerFragment extends BaseFragment {
    private final String TAG = "collectorContainer";
    private CollectorFragment mCollectorFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        if (mCollectorFragment == null) {
            mCollectorFragment = new CollectorFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if(mCollectorFragment.isAdded()) {
            Log.i(TAG, "mCollectorFragment is added");
            transaction.show(mCollectorFragment);
        } else {
            Log.i(TAG, "mCollectorFragment add");
            transaction.add(R.id.collector_container, mCollectorFragment, "CollectorFragment");
        }

        transaction.commit();
        return inflater.inflate(R.layout.collector_container, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "View Created");

    }


    public CollectorFragment getCollectorFragment(){
        return mCollectorFragment;
    }

}
