package com.koala.timemaster;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by jq.zeng on 8/16/15.
 */
public class MoreTagsSelectAdapter extends BaseExpandableListAdapter {

    private final String TAG = "MoreTagAdapter";
    private List<Role> mTags;
    private Context mContext;
    private LayoutInflater inflater;
    private TagOps tagOps;

    public MoreTagsSelectAdapter(Context context, List<Role> roleList) {
        mContext = context;
        mTags = roleList;
        inflater = LayoutInflater.from(mContext);
        tagOps = new TagFileOps(context);
    }

    public MoreTagsSelectAdapter() {
        inflater = LayoutInflater.from(mContext);
    }

    /**
     * View Holder for group view.
     */
    private class GroupViewHolder {
        ImageView mImageView;
        TextView mTagView;
        CheckBox mCheckBox;
    }

    /**
     * ViewHolder for child view.
     */
    private class ChildViewHolder{
        RatingBar mRatingBar;
    }

    // This Function used to inflate parent rows view

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parentView) {
        final GroupViewHolder viewHolder;

        Log.i(TAG, "get group view");
        // Inflate grouprow.xml file for collectorEvent rows
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.more_tags_role_selected, parentView, false);
            viewHolder = new GroupViewHolder();
            viewHolder.mImageView = (ImageView)convertView.findViewById(R.id.more_image);
            viewHolder.mTagView = (TextView) convertView.findViewById(R.id.more_tag);
            viewHolder.mCheckBox = (CheckBox) convertView.findViewById(R.id.more_tag_choose);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupViewHolder)convertView.getTag();
        }

        // Get grouprow.xml file elements and set values
        viewHolder.mTagView.setText(mTags.get(groupPosition).getTag());


        viewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                viewHolder.mCheckBox.setChecked(isChecked);
                mTags.get(groupPosition).setChecked(isChecked);

                //update tags list in the activity
                if (mContext instanceof MoreTagsNewActivity) {
                    tagOps.saveTagsToFile(mTags);
                    ((MoreTagsNewActivity) mContext).saveTags(mTags);
                }
                Toast.makeText(mContext, isChecked ? "is checked" : "is unchecked", Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }


    // This Function used to inflate child rows view
    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parentView) {
        ChildViewHolder viewHolder;
        Log.i(TAG, "get child view");

        // Inflate childrow.xml file for collectorSubEvent rows
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.more_tags_sub, parentView, false);
            viewHolder = new ChildViewHolder();
            viewHolder.mRatingBar = (RatingBar) convertView.findViewById(R.id.more_tag_rating);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ChildViewHolder) convertView.getTag();
        }

        viewHolder.mRatingBar.setRating(mTags.get(groupPosition).getWeight());
        viewHolder.mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mTags.get(groupPosition).setWeight((int) rating);

                //update tagslist in the activity
                if (mContext instanceof MoreTagsNewActivity) {
                    ((MoreTagsNewActivity)mContext).saveTags(mTags);
                }
                Toast.makeText(mContext,mTags.get(groupPosition).getTag() + "rating change to" + rating, Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return 3;
    }

    //Call when child row clicked
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }


    @Override
    public Object getGroup(int groupPosition) {
        return mTags.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mTags.size();
    }

    //Call when parent row clicked
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public void notifyDataSetChanged() {
        // Refresh List rows
        super.notifyDataSetChanged();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

}
