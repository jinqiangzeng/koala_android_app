package com.koala.timemaster;

import com.example.jqzeng.mylibrary.event.EventModel;

import java.util.Calendar;
import java.util.List;

/**
 * Created by jq.zeng on 8/17/15.
 */
 /**
 * communicate between Activity and collectorFragment
 */
public interface EventOps {
    /**
     * load Evnets between start and end date
     * @param start
     * @param end
     * @return
     */
    List<EventModel> loadCalendarEvents(Calendar start, Calendar end);

    /**
     * add a specific event to list
     * @param newEvent
     * @return
     */
    List<EventModel> addCalendarEvent(CollectorEvent newEvent);
}
