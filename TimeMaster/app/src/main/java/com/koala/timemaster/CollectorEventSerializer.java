package com.koala.timemaster;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by jq.zeng on 10/3/15.
 */
public class CollectorEventSerializer implements JsonSerializer<List<CollectorEvent>>{

        @Override
        public JsonElement serialize(List<CollectorEvent> listCollectorEvent, Type typeOfSrc, JsonSerializationContext context) {
            final JsonArray jsonArray = new JsonArray();
            CollectorEvent collectorEvent;
            String title,  location, description;
            for (int i = 0; i < listCollectorEvent.size(); i++ ){
                JsonObject jsonObject = new JsonObject();
                collectorEvent = listCollectorEvent.get(i);
                if ((title = collectorEvent.getTitle())== null){
                    title = "null";
                }
                if ((location = collectorEvent.getLocation()) == null) {
                    location = "null";
                }
                if ((description = collectorEvent.getDescription()) == null) {
                    description = "null";
                }

                jsonObject.addProperty("Title",title);
                jsonObject.addProperty("Role", collectorEvent.getRole());
                jsonObject.addProperty("Location", location);
                jsonObject.addProperty("Priority", collectorEvent.getPriority().toString());
                jsonObject.addProperty("Description", description);
                jsonArray.add(jsonObject);
            }
            return jsonArray;
        }

}