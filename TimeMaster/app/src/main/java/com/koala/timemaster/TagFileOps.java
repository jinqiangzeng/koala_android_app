package com.koala.timemaster;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jq.zeng on 10/12/15.
 */

/**
 * TagFileOps implement tag related method in a very basic, simple way
 */
public class TagFileOps implements TagOps {
    private final String TAG = "TagFileOps";
    private final String TAGS_FILE_NAME = "tags3.txt";
    private Context mContext;

    /**
     * openFIleInput has to be run on certain context, initialize the context here
     * @param context
     */
    TagFileOps(Context context){
        mContext = context;
    }

    /**
     * load tags from disk file
     * @return
     */
    public List<Role> loadTagsFromFile() {
        List<Role> storedTags = new ArrayList<>();
        BufferedReader reader = null;
        try {
            FileInputStream fis = mContext.openFileInput(TAGS_FILE_NAME);
            reader = new BufferedReader(new InputStreamReader(fis));

            String tag = null;
            String weight = null;

            while (null != (tag = reader.readLine())) {
                weight = reader.readLine();

                Log.d(TAG, "load " + "tag: " + tag + " weight: " + weight + "\n");
                storedTags.add(new Role(tag, Integer.parseInt(weight),false));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return storedTags;
    }

    /**
     * save tags to disk
     * @param tags
     */
    public void saveTagsToFile(List<Role> tags){
        PrintWriter writer = null;
        try {
            //overwrite the events
            FileOutputStream fos = mContext.openFileOutput(TAGS_FILE_NAME, mContext.MODE_PRIVATE);
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    fos)));

            for (int idx = 0; idx < tags.size(); idx++) {
                writer.println(tags.get(idx).saveFormatString());
                Log.d(TAG, "sv " + tags.get(idx).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != writer) {
                writer.close();
            }
        }
    }

    /**
     * TagFileOps won't save any info in memory, use MoreTagsNewActivity's method
     * @return
     */
    public List<Role> loadTags() {
        return null;
    }

    public List<Role> addTag(Role role) {
        return null;
    }
}
