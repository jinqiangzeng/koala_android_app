package com.koala.timemaster;

import android.content.Intent;
import android.location.Location;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.jqzeng.mylibrary.event.EventModel;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.datatype.Duration;

/**
 * Created by jq.zeng on 6/26/15.
 */
public class CollectorEvent extends EventModel implements Comparator<CollectorEvent>{
    private static final String TAG = "CollectorEvent";
    private Calendar mCreateDate;
    private Calendar mDeadline;
    private List<ContactsContract> participators;
    private String mRole;
    private Priority mPriority; /*< */
    private boolean checked;
    private int progress;
    private List<String> stringList;  /*< display in the listview*/
    private int nice; // 1-100

    public enum Priority{
        Low,Medium,High
    }

    /**
     * construct CollectorEvent manually
     * @param title
     * @param role
     * @param location
     * @param priority
     * @param note
     */
    CollectorEvent(String title,String role,String location, Priority priority,String note) {
        stringList = new ArrayList<String>();

        setTitle(title);
        this.mRole = role;
        this.mPriority = priority;
        setDescription(note);

        if (role != null) {
            stringList.add(role);
        }
        if (location != null)
            setLocation(location);
        if (priority != null) {
            stringList.add(priority.toString());
        }
        if (getDescription() != null) {
            stringList.add(getDescription());
        }
    }

    CollectorEvent() {
        super();
        this.mRole = "Personal Developing";
        this.mPriority = Priority.Low;
        this.stringList = new ArrayList<String>();
        this.mCreateDate = Calendar.getInstance();
    }

    /**
     * construct CollectorEvent from intent
     * @param intent
     */
    CollectorEvent(Intent intent) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.getDefault());
        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();
        String date;
        stringList = new ArrayList<String>();
        setTitle(intent.getStringExtra("TITLE"));
        setRole(intent.getStringExtra("ROLE")) ;
        setLocation(intent.getStringExtra("LOCATION"));
        setPriority(Priority.valueOf(intent.getStringExtra("PRIORITY"))) ;

        try {
            date = intent.getStringExtra("STARTDATE");
            Log.d(TAG, date);
            if (!date.equals("null")) {
                calendarStart.setTime(simpleDateFormat.parse(date));
                setStartDate(calendarStart);
            }
        }catch( ParseException e){
                e.printStackTrace();
        }

        try{
            date = intent.getStringExtra("ENDDATE");
            Log.d(TAG, date);
            if (!date.equals("null")) {
                calendarEnd.setTime(simpleDateFormat.parse(date));
                setEndDate(calendarEnd);
            }
        }catch( ParseException e){
            e.printStackTrace();
        }

    }
    public Calendar getCreateDate() {return mCreateDate;}

    public void setCreateDate(Calendar date) { mCreateDate = date;}


    public boolean isChecked()
    {
        return checked;
    }
    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }

    public int getDisplayItems(){
        return stringList.size();
    }

    public String getRole() {
        return mRole;
    }

    public CollectorEvent setRole(String role) {
        mRole = role;
        if (role != null)
            stringList.add(role);
        return this;
    }

    public Priority getPriority() {
        return mPriority;
    }

    public CollectorEvent setPriority(Priority priority) {
        mPriority = priority;
        if (priority != null)
            stringList.add(priority.toString());
        return this;
    }

    public CollectorEvent setLocation(String location) {
        super.setLocation(location);
        if (location != null) {
            stringList.add(location);
        }
        return this;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public String toString() {
        return getTitle() + "\n" + mRole+ "\n" + getLocation()+ "\n" + mPriority.toString()+ "\n" + getDescription();
    }


    @Override
    public int compare(CollectorEvent lhs, CollectorEvent rhs) {
        return lhs.nice - rhs.nice;
    }

    public static Comparator<CollectorEvent> CollectorEventComparator = new Comparator<CollectorEvent>() {
        @Override
        public int compare(CollectorEvent lhs, CollectorEvent rhs) {
            return lhs.nice - rhs.nice;
        }
    };
}
