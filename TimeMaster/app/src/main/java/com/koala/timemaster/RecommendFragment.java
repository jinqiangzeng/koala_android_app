package com.koala.timemaster;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;
//import com.squareup.okhttp.OkHttpClient;

/**
 * Created by jq.zeng on 6/22/15.
 */
public class RecommendFragment extends BaseFragment {
    private final String TAG = "Recommend Fragment";
    private Button button;
    private TextView mTextView;
 //   private OkHttpClient client;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recommend, container, false);

        mTextView = (TextView) view.findViewById(R.id.webtext);

 //       client = new OkHttpClient();
        button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "click button", Toast.LENGTH_SHORT).show();
               // startActivity(new Intent(getActivity(), CalendarEventTestActivity.class));
                JsonArray jsonArray = new JsonArray();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("event_title", "Lunar Eclipse");
                jsonObject.addProperty("event_place", "home");
                jsonObject.addProperty("event_start", "9/25/2015 20:00");
                jsonObject.addProperty("event_end", "9/25/2015 21:30");
                jsonArray.add(jsonObject);

                JsonObject jsonObject1 = new JsonObject();
                jsonObject1.addProperty("event_title", "Back to China");
                jsonObject1.addProperty("event_place", "SFO");
                jsonObject1.addProperty("event_start", "2/1/2016 9:00");
                jsonObject1.addProperty("event_end", "2/15/2016 9:00");
                jsonArray.add(jsonObject1);

                Ion.with(getActivity().getApplicationContext())
                        .load("POST","http://192.168.0.17:8080/mobilecalendar")
//                        .setJsonObjectBody(jsonObject)
                        .setJsonArrayBody(jsonArray)
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                if (result != null) {
                                    Log.i(TAG, result.getResult());
                                } else {
                                    Log.i(TAG, "result is null");
                                }
                            }
                        });
//                        .asJsonObject()
//                        .setCallback(new FutureCallback<JsonObject>() {
//                            @Override
//                            public void onCompleted(Exception e, JsonObject result) {
//                                if (result != null) {
//                                    Log.i(TAG, result.toString());
//                                } else {
//                                    Log.i(TAG, "result is null");
//                                }
//                            }
//                        });
            }
        });

        return view;
    }

    private class RecommendListAdapter extends BaseAdapter{

    }

}
