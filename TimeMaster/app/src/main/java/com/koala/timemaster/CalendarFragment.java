package com.koala.timemaster;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.Toast;

import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jq.zeng on 6/21/15.
 */
public class CalendarFragment extends BaseFragment {
    private final static String TAG = "CalendarFragment";
    private ChangeCalendarView mCallback;


    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            mCallback = (ChangeCalendarView) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CalendarContainerFragment.ChangeCalendarView");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calendar_month_view, container, false);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        CalendarPickerView calendar = (CalendarPickerView) view.findViewById(R.id.calendar_view);
        final Date today = new Date();
        calendar.init(lastYear.getTime(), nextYear.getTime())
                .withSelectedDate(today);
        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                Calendar tmp = Calendar.getInstance();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" MMMM/dd/yyyy", Locale.getDefault());
                if (!simpleDateFormat.format(date).equals(simpleDateFormat.format(today))) {
                    Log.i(TAG, "today:" + today.toString() + " date:" + date.toString());
                    tmp.setTime(date);
                }
                Log.i(TAG, "setDisplayDate:" + tmp.get(Calendar.DAY_OF_MONTH) + tmp.get(Calendar.HOUR_OF_DAY));
                mCallback.setDisplayDate(tmp);
                mCallback.changeCalendarView(CalendarContainerFragment.FragmentToShow.Week);
            }

            @Override
            public void onDateUnselected(Date date) {
                Log.i(TAG, "onDateUnselected");
            }
        });

        return view;

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
