package com.koala.timemaster;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.example.jqzeng.mylibrary.IconPagerAdapter;
import com.example.jqzeng.mylibrary.IconTabPageIndicator;
import com.example.jqzeng.mylibrary.calendar.CalendarModel;
import com.example.jqzeng.mylibrary.calendar.CalendarsProxy;
import com.example.jqzeng.mylibrary.event.EventModel;
import com.example.jqzeng.mylibrary.event.EventsProxy;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class MainActivity extends FragmentActivity
        implements ChangeCalendarView, CalendarsProxy.Listener, EventOps,
        EventsProxy.Listener {
    private final static String TAG = "MainActivity";
    private ViewPager mViewPager;
    private IconTabPageIndicator mIndicator;
    private FragmentAdapter mAdapter;

    private EventsProxy mEventsQueryHandler;
    private List<EventModel> mEvents;
    private CalendarsProxy mCalendarsQueryHandler;
    private int mCalendarID;

    private Calendar displayDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(TAG, "onCreate");

        if(mEvents == null)
            mEvents = new ArrayList<EventModel>();

        displayDate = Calendar.getInstance();

        //query native Calendar event
        mCalendarsQueryHandler = new CalendarsProxy(getContentResolver());
        mCalendarsQueryHandler.registerListener(this);
        mCalendarsQueryHandler.getAll(CalendarModel.create(), null, null);

        mEventsQueryHandler = new EventsProxy(getContentResolver());
        mEventsQueryHandler.registerListener(this);
        queryEvents();

        initViews();

        setAlarms();
    }

    private void initViews() {
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mIndicator = (IconTabPageIndicator) findViewById(R.id.indicator);
        List<BaseFragment> fragments = initFragments();
        mAdapter = new FragmentAdapter(fragments, getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        mIndicator.setVisibility(View.VISIBLE);
    }

    private void setAlarms(){
        AlarmManager[] mAlarmManager = new AlarmManager[5];
        Calendar[] mCalendar = new Calendar[5];
        PendingIntent[] mAlarmIntent = new PendingIntent[5];
        Intent intent = new Intent(MainActivity.this, PriorityCalculator.class); //intent for priority calculation
        Intent uploadIntent = new Intent(MainActivity.this, UploadService.class); //intent for upload calendar to server

        //set Alarm, calculate priority at 8:30am, 11:30am, 2:30pm, 5:30pm
        for (int i=0; i < 5; i++){
            mAlarmManager[i] = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            mCalendar[i] = Calendar.getInstance();
            mCalendar[i].set(Calendar.MINUTE, 30);
            mAlarmIntent[i] = PendingIntent.getBroadcast(MainActivity.this, i, intent, 0);
        }
        mAlarmIntent[4] = PendingIntent.getBroadcast(MainActivity.this,4,uploadIntent, 0);  //overwrite upload intent

        mCalendar[0].set(Calendar.HOUR_OF_DAY, 8);
        mCalendar[1].set(Calendar.HOUR_OF_DAY, 11);
        mCalendar[2].set(Calendar.HOUR_OF_DAY, 13);
        mCalendar[3].set(Calendar.HOUR_OF_DAY, 16);
        mCalendar[4].set(Calendar.HOUR_OF_DAY, 00);   //upload calendars at midnight

        for(int i=0; i < 5; i++){
            mAlarmManager[i].setInexactRepeating(AlarmManager.RTC_WAKEUP, mCalendar[i].getTimeInMillis(), AlarmManager.INTERVAL_DAY, mAlarmIntent[i]);
        }

    }

    private List<BaseFragment> initFragments() {
        List<BaseFragment> fragments = new ArrayList<BaseFragment>();

//        CalendarFragment calendarFragment = new CalendarFragment();
//        calendarFragment.setTitle(getResources().getString(R.string.calendar_bar));
//        calendarFragment.setIconId(R.drawable.tab_user_selector);
//        fragments.add(calendarFragment);
        CalendarContainerFragment calendarContainerFragment = new CalendarContainerFragment();
        calendarContainerFragment.setTitle(getResources().getString(R.string.calendar_bar));
        calendarContainerFragment.setIconId(R.drawable.tab_user_selector);
        fragments.add(calendarContainerFragment);

        CollectorFragment collectorFragment = new CollectorFragment();
        collectorFragment.setTitle(getResources().getString(R.string.collector_bar));
        collectorFragment.setIconId(R.drawable.tab_record_selector);
        fragments.add(collectorFragment);
//        CollectorContainerFragment collectorContainerFragment = new CollectorContainerFragment();
//        collectorContainerFragment.setTitle(getResources().getString(R.string.calendar_bar));
//        collectorContainerFragment.setIconId(R.drawable.tab_record_selector);
//        fragments.add(collectorContainerFragment);

        RecommendFragment recommendFragment = new RecommendFragment();
        recommendFragment.setTitle(getResources().getString(R.string.recommend_bar));
        recommendFragment.setIconId(R.drawable.tab_user_selector);
        fragments.add(recommendFragment);

        MoreFragment meFragment = new MoreFragment();
        meFragment.setTitle(getResources().getString(R.string.more_bar));
        meFragment.setIconId(R.drawable.tab_record_selector);
        fragments.add(meFragment);

        return fragments;
    }

    class FragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
        private List<BaseFragment> mFragments;

        public FragmentAdapter(List<BaseFragment> fragments, FragmentManager fm) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getIconResId(int index) {
            return mFragments.get(index).getIconId();
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragments.get(position).getTitle();
        }
    }

//    public void onNewEventAdd(CollectorEvent collectorEvent) {
//        Log.i("MainActivity", "NewEventAdd Callback");
//        CollectorFragment collectorFragment = ((CollectorContainerFragment) (mAdapter.mFragments.get(1))).getCollectorFragment();
//        collectorFragment.addRoleEvent(collectorEvent);
//        getSupportFragmentManager().beginTransaction().replace(R.id.collector_container, collectorFragment).addToBackStack(null).commit();
//    }



    /**
     * switch CalendarFragment view between day view and month view.
     * @param fragmentToShow
     */
    public void changeCalendarView(CalendarContainerFragment.FragmentToShow fragmentToShow){
        Log.i("MainActiivty", "changeCalendarView Callback");
        CalendarContainerFragment calendarContainerFragment = (CalendarContainerFragment)(mAdapter.mFragments.get(0));
        calendarContainerFragment.setFragmentToShow(fragmentToShow);
    }

    /**
     * load events of native calendar between start and end
     * @param start
     * @param end
     * @return
     */
    public List<EventModel> loadCalendarEvents(Calendar start, Calendar end) {
        List<EventModel> eventModels = new ArrayList<EventModel>();
        if (mEvents != null) {
            for (EventModel event : mEvents) {
                Calendar calendar = event.getStartDate();
                if (calendar == null) {
                    Log.e(TAG, "startDate is null " + event.toString());
                } else if (calendar.after(start) && calendar.before(end)) {
                    eventModels.add(event);
                }
            }
        }
        return eventModels;
    }

    /**
     * add new event to list and write it to native calendar.
     * @param newEvent
     * @return
     */
    public List<EventModel> addCalendarEvent(CollectorEvent newEvent) {
        EventModel event = EventModel.create();
        event.setTitle(newEvent.getTitle());
        event.setLocation(newEvent.getLocation());
        event.setDescription(newEvent.getRole());
        event.setStartDate(newEvent.getStartDate());
        event.setEndDate(newEvent.getEndDate());

        if(mEvents == null)
            mEvents = new ArrayList<EventModel>();
        mEvents.add(event);

        mEventsQueryHandler.create(EventModel.create()
                .setCalendarId(mCalendarID)
                .setTitle(event.getTitle())
                .setDescription(event.getDescription())
                .setStartDate(event.getStartDate())
                .setEndDate(event.getEndDate())
                .setAccessLevel(CalendarContract.Events.ACCESS_PUBLIC)
                .setTimeZone("GMT"));

        return mEvents;
    }

    /**
     * set display date for day view
     * @param day
     */
    public void setDisplayDate(Calendar day) {
        Log.i(TAG, "setDisplayDate:" + day.get(Calendar.DAY_OF_MONTH) + day.get(Calendar.HOUR_OF_DAY));
        displayDate = day;
    }

    /**
     * get display date of day view
     * @return
     */
    public Calendar getDisplayDate() {
        Log.i(TAG, "getDisplayDate: date:" + displayDate.get(Calendar.DAY_OF_MONTH) +" Hour"+ displayDate.get(Calendar.HOUR_OF_DAY));
        return displayDate;
    }

    public void queryEvents() {
        Log.i("Event_QUERY", "starting event query");
        Calendar start = Calendar.getInstance();
        Log.i(TAG, start.toString());
        start.add(Calendar.MONTH, -1);
        Calendar end = Calendar.getInstance();
        end.add(Calendar.MONTH, 2);
        this.mEventsQueryHandler.get(EventModel.create(), start, end);
    }

    //Events related
    @Override
    public void onEventsRetrieved(List<EventModel> events) {
        if(mEvents != null && events != null) {
            for (EventModel em : events) {
                if (!mEvents.contains(em)) {
                    mEvents.add(em);
                }
            }
            //mEvents.clear();
            //mEvents.addAll(events);
            String string = "";
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.getDefault());

            for (EventModel em : mEvents) {
                string += em.getTitle() + "  " + em.getLocation() + "  from" + df.format(em.getStartDate().getTime()) + " to" + df.format(em.getEndDate().getTime()) + "\n";
            }
        }
    }

    @Override
    public void onEventCreated() {
        Log.i(TAG,"Calendar onEventCreated");
    }

    @Override
    public void onEventDeleted() {
        Log.i(TAG, "Calendar onEventDeleted");
    }

    @Override
    public void onEventUpdated() {
        Log.i(TAG, "Calendar onEventUpdated");
    }

    @Override
    public void onCalendarCreated() {
        Log.i(TAG, "Calendar onCalendarCreated");
    }

    @Override
    public void onCalendarDeleted() {
        Log.i(TAG, "Calendar onCalendarDeleted");
    }

    @Override
    public void onCalendarsRetrieved(List<CalendarModel> calendars) {
        Log.i(TAG, "onCalendarRetrieved");

        boolean isCalendarExist = false;
        int position = 0;
        for (CalendarModel cm : calendars) {
            if (cm.getAccountName() != null && cm.getAccountName().equals(getResources().getString(R.string.calendar_name))){
                isCalendarExist = true;
                mCalendarID = position;
            }
            position++;
        }

        // add calender id
        if (!isCalendarExist) {
            mCalendarID = position;
            String name = getResources().getString(R.string.calendar_name);
            Log.i(TAG,name);

            mCalendarsQueryHandler.create(CalendarModel.create()
                    .setVisibility(true)
                    .setName(name)
                    .setDisplayName(name));
        }
    }

}
