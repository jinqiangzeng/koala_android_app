package com.koala.timemaster;

import java.util.Calendar;

/**
 * Created by jq.zeng on 10/7/15.
 */
public class MetaData {
    private Calendar calendarUploadDate; /*< indicate the latest time uploading native calendar and CollectorEvents to server*/
    private Calendar collectorEventsUploadDate;

    public void setCalendarUploadDate(Calendar date) {
        calendarUploadDate = date;
    }

    public Calendar getCalendarUploadDate() {
        return calendarUploadDate;
    }

    public void setCollectorEventsUploadDate(Calendar date) {
        calendarUploadDate = date;
    }

    public Calendar getCollectorEventsUploadDate() {
        return calendarUploadDate;
    }
}
