package com.koala.timemaster;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jq.zeng on 8/16/15.
 */
public class MoreTagsNewActivity extends ExpandableListActivity implements TagOps{
    private final String TAG = "MoreTagsNewActivity";
    private ExpandableListView mExpandableListView;
    private MoreTagsSelectAdapter mMoreTagsSelectAdapter;
    private MoreTagsNormalAdapter mMoreTagsNormalAdapter;
    private List<Role> mTags;
    private TextView mBackTextView;
    private TextView mSelectView;
    private TextView mDeleteView;
    private TagOps tagOps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_tags_activity);

        mTags = new ArrayList<Role>();
        tagOps = new TagFileOps(this);

        mTags = tagOps.loadTagsFromFile();
        if (mTags.size() == 0) {
            mTags.add(new Role("Personal Developing", 5, false));
            mTags.add(new Role("Family", 3, false));
            tagOps.saveTagsToFile(mTags);
        }

        mExpandableListView = (ExpandableListView) findViewById(android.R.id.list);
        mExpandableListView.setGroupIndicator(null);
        mExpandableListView.setClickable(true);
        mMoreTagsSelectAdapter = new MoreTagsSelectAdapter(getApplicationContext(), mTags);
//        mExpandableListView.setAdapter(mAdapter);
        mMoreTagsNormalAdapter = new MoreTagsNormalAdapter(getApplicationContext(), mTags);
        setListAdapter(mMoreTagsNormalAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.create_tag);
        fab.attachToListView(mExpandableListView);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TagNewDialogFragment NewTag = new TagNewDialogFragment();
                NewTag.show(getFragmentManager(), "NewTag");
                Log.d(TAG, "create_tag clicked");
            }
        });

        mBackTextView = (TextView) findViewById(R.id.back_to_more);
        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreTagsNewActivity.this, MainActivity.class);

                intent.putParcelableArrayListExtra("tags", (ArrayList<Role>) mTags);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        mDeleteView = (TextView) findViewById(R.id.tags_delete);
        mDeleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDeleteView.getText().toString().equals("Cancel")) {
                    mSelectView.setText("Select");
                    mDeleteView.setText("");
                    setListAdapter(mMoreTagsNormalAdapter);
                }
            }
        });

        mSelectView = (TextView) findViewById(R.id.tags_select);
        mSelectView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSelectView.getText().toString().equals("Select")){
                    mSelectView.setText("Delete");
                    mDeleteView.setText("Cancel");
                    setListAdapter(mMoreTagsSelectAdapter);
                } else {
                    mSelectView.setText("Select");
                    mDeleteView.setText("");
                    for(int i = 0; i < mTags.size(); i++) {
                        Role role = mTags.get(i);
                        Log.d(TAG, role.getTag() + (role.isChecked() ? "  checked" : "  not checked"));

                        //remove the selected tags
                        if (role.isChecked())
                            mTags.remove(i);
                    }
                    mMoreTagsNormalAdapter.notifyDataSetChanged();
                    mMoreTagsSelectAdapter.notifyDataSetChanged();
                    setListAdapter(mMoreTagsNormalAdapter);
                    tagOps.saveTagsToFile(mTags);

                }

            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        tagOps.saveTagsToFile(mTags);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mTags == null) {
            mTags = tagOps.loadTagsFromFile();
        }
    }

    public void saveTags(List<Role> roleList) {
        mTags = roleList;
    }

    public Role deleteTag(int position) {
        return mTags.remove(position);
    }

    public boolean deleteTag(Role role) {
        return mTags.remove(role);
    }

    /**
     * load tags from file "tags.txt", use TagFileOps method
     * @return
     */
    public List<Role> loadTagsFromFile(){
        return tagOps.loadTagsFromFile();
    }

    /**
     * use TagFileOps method
     */
    public void saveTagsToFile(List<Role> tags) {
        tagOps.saveTagsToFile(tags);
    }
    /**
     *
     * @return
     */
    public List<Role> loadTags() {
        return mTags;
    }

    public List<Role> addTag(Role role) {
        mTags.add(role);
        return mTags;
    }
}
