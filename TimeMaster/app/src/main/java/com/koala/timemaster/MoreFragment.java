package com.koala.timemaster;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jqzeng.mylibrary.calendar.CalendarModel;
import com.example.jqzeng.mylibrary.calendar.CalendarsProxy;
import com.example.jqzeng.mylibrary.event.EventModel;
import com.example.jqzeng.mylibrary.event.EventsProxy;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by jq.zeng on 6/22/15.
 */
public class MoreFragment extends BaseFragment
{
    private final String TAG = "MoreFragment";
    private TextView mAccountTextView;
    private TextView mTagsTextView;
    private TextView mSettingsTextView;
    private final int MORE_TAGS_CODE = 5;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.more, container, false);

        mAccountTextView = (TextView) view.findViewById(R.id.account);
        mAccountTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "account clicked");
            }
        });

        mTagsTextView = (TextView) view.findViewById(R.id.tags);
        mTagsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MoreTagsNewActivity.class);
                startActivityForResult(intent,MORE_TAGS_CODE);
                Log.i(TAG, "tags clicked");
            }
        });

        mSettingsTextView = (TextView) view.findViewById(R.id.settings);
        mSettingsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "setting clicked");
            }
        });

        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG, "Entered onActivityResult()");

        if (requestCode == MORE_TAGS_CODE) {
            if(resultCode == getActivity().RESULT_OK) {
                List<Role> myTags = data.getParcelableArrayListExtra("tags");
                for (Role x : myTags) {
                    Log.i(TAG, x.toString());
                }
            }
        }
    }
}
