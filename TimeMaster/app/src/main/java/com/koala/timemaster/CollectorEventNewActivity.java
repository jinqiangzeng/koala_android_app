package com.koala.timemaster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jq.zeng on 7/16/15.
 */
public class CollectorEventNewActivity extends FragmentActivity{
    private static final String TAG = "CollectorEventNew";
    private EditText mTitle;
    private EditText mLocation;
    private RadioGroup mPriority;
    private CollectorEvent mCollectorEventNew;
    private TextView mEventStart;
    private TextView mEventEnd;
    private SlideDateTimeListener mSlideDateTimeListener;
    private Calendar mStartDate;
    private Calendar mEndDate;
    private boolean mStartSet;
    private boolean mEndSet;
    private SimpleDateFormat startFormat;
    private SimpleDateFormat endFormat;
    private Role mRole;
    private final String TAGS_FILE_NAME = "tags2.txt";
    private List<Role> mTags;

    @Override
    public void  onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.collector_event_new);
        mCollectorEventNew = new CollectorEvent();
        mTitle = (EditText) findViewById(R.id.event_new_title);
        mLocation = (EditText)findViewById(R.id.event_new_location);
        mPriority = (RadioGroup) findViewById(R.id.event_new_priorityGroup);
        mEventStart = (TextView) findViewById(R.id.time_and_date_start);
        mEventEnd = (TextView) findViewById(R.id.time_and_date_end);
        mStartSet = false;
        mEndSet = false;
        startFormat = new SimpleDateFormat("EEE MMM/dd/yyyy HH:mm", Locale.getDefault());
        endFormat = new SimpleDateFormat("HH:mm",Locale.getDefault());

        final SlideDateTimeListener startSlideDateTimeListener = new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date) {
                // Do something with the date. This Date object contains
                // the date and time that the user has selected.
                Toast.makeText(getApplicationContext(), startFormat.format(date), Toast.LENGTH_SHORT).show();
                mStartDate = Calendar.getInstance();
                mStartDate.setTime(date);
                mEventStart.setText(startFormat.format(date));
                mStartSet = true;
                mEndDate = (Calendar)mStartDate.clone();
                mEndDate.add(Calendar.HOUR_OF_DAY, 1);

                mEventEnd.setText(endFormat.format(mEndDate.getTime()));
            }

            @Override
            public void onDateTimeCancel()
            {
                // Overriding onDateTimeCancel() is optional.

            }
        };

        final SlideDateTimeListener endSlideDateTimeListener = new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date) {
                Toast.makeText(getApplicationContext(), endFormat.format(date), Toast.LENGTH_SHORT).show();
                if (mEndDate == null){
                    mEndDate = Calendar.getInstance();
                }
                mEndDate.setTime(date);
                mEventEnd.setText(endFormat.format(date));
                mEndSet = true;
            }

            @Override
            public void onDateTimeCancel()
            {
                // Overriding onDateTimeCancel() is optional.

            }
        };


        Calendar today = Calendar.getInstance();
        today.add(Calendar.HOUR_OF_DAY, 1);

        mEventStart.setText(startFormat.format(today.getTime()));
        mEventStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(startSlideDateTimeListener)
                        .setInitialDate(new Date())
                        .setIs24HourTime(true)
                        .build()
                        .show();
            }
        });

        today.add(Calendar.HOUR_OF_DAY, 1);
        mEventEnd.setText(endFormat.format(today.getTime()));
        mEventEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date;
                if (mStartDate == null) {
                    date = new Date();
                } else {
                    Calendar calendar = (Calendar) mStartDate.clone();
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    date = calendar.getTime();
                }
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(endSlideDateTimeListener)
                        .setInitialDate(date)
                        .setIs24HourTime(true)
                        .build()
                        .show();
            }
        });

        final Button mCancel = (Button) findViewById(R.id.event_new_cancelButton);
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO - Indicate result and finish
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        final Button mDone = (Button) findViewById(R.id.event_new_submitButton);
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCollectorEventNew.setPriority(getPriority());
                mCollectorEventNew.setTitle(getEventTitle());
                mCollectorEventNew.setLocation(getEventLocation());
                mCollectorEventNew.setRole(getTag());

                //only start date is set
                if(mStartSet || mEndSet){
                    mCollectorEventNew.setStartDate(mStartDate);
                    mCollectorEventNew.setEndDate(mEndDate);
                }

                Log.d(TAG, getEventTitle() + " " + getEventLocation() + " " + getPriority().toString());
                Toast.makeText(CollectorEventNewActivity.this, mCollectorEventNew.toString(), Toast.LENGTH_LONG).show();

                // Package ToDoItem data into an Intent
                Intent data = new Intent();

                //H 0-23, hh 12 o'clock
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.getDefault());

                data.putExtra("TITLE", mCollectorEventNew.getTitle());
                data.putExtra("ROLE", mCollectorEventNew.getRole());
                data.putExtra("PRIORITY", mCollectorEventNew.getPriority().toString());
                data.putExtra("LOCATION",mCollectorEventNew.getLocation());

                if (mStartSet || mEndSet){
                    data.putExtra("STARTDATE", simpleDateFormat.format(mCollectorEventNew.getStartDate().getTime()));
                    data.putExtra("ENDDATE", simpleDateFormat.format(mCollectorEventNew.getEndDate().getTime()));

                } else {
                    data.putExtra("STARTDATE", "null");
                    data.putExtra("ENDDATE", "null");
                }
                // TODO - return data Intent and finish
                setResult(RESULT_OK, data);
                finish();
            }
        });

        TagOps tagOps = new TagFileOps(this);
        Spinner mSpinner = (Spinner) findViewById(R.id.spinner);
        ArrayList<Role> tags = (ArrayList<Role>)tagOps.loadTagsFromFile();
        ArrayAdapter<Role> adapter = new ArrayAdapter<Role>(this,android.R.layout.simple_spinner_dropdown_item,tags);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),parent.getSelectedItem().toString(),Toast.LENGTH_SHORT).show();
                mRole = (Role)parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getTag() {
        return mRole.getTag();
    }

    private String getEventTitle() {
        return mTitle.getText().toString();
    }

    private String getEventLocation(){
        return mLocation.getText().toString();
    }

    private CollectorEvent.Priority getPriority() {
        switch(mPriority.getCheckedRadioButtonId()){
            case R.id.event_new_lowPriority:
                return CollectorEvent.Priority.Low;
            case R.id.event_new_medPriority:
                return CollectorEvent.Priority.Medium;
            case R.id.event_new_highPriority:
                return CollectorEvent.Priority.High;
            default:
                return CollectorEvent.Priority.Low;
        }
    }
}
