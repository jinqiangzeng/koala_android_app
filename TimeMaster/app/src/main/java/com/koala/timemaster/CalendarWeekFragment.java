package com.koala.timemaster;

import android.app.Activity;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.example.jqzeng.mylibrary.event.EventModel;
import com.squareup.timessquare.CalendarCellView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by jq.zeng on 6/25/15.
 */
public class CalendarWeekFragment extends BaseFragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventLongPressListener {
    private final static String TAG = "CalendarWeek";

    private WeekView mWeekView;
    private TextView mTextView;
    private List<EventModel> mEvents;
    private List<Integer> eventColors;
    private Calendar displayDate;
    private List<CalendarCellView> weekdaysRow;

    private ChangeCalendarView mChangeCalendarView;
    private EventOps mEventOps;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            mChangeCalendarView = (ChangeCalendarView) activity;
            mEventOps = (EventOps) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CalendarContainerFragment.ChangeCalendarView");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(mEvents == null)
            mEvents = new ArrayList<EventModel>();

        //load events in 1 Month
        Calendar startTime = Calendar.getInstance();
        startTime.add(Calendar.MONTH, -1);
        Calendar endTime = Calendar.getInstance();
        endTime.add(Calendar.MONTH, 2);
        mEvents = mEventOps.loadCalendarEvents(startTime,endTime);

        //init display colar
        eventColors = new ArrayList<Integer>();

        eventColors.add(getResources().getColor(R.color.event_color_01));
        eventColors.add(getResources().getColor(R.color.event_color_02));
        eventColors.add(getResources().getColor(R.color.event_color_03));
        eventColors.add(getResources().getColor(R.color.event_color_04));

        //set display date
        displayDate = mChangeCalendarView.getDisplayDate();

        weekdaysRow = new ArrayList<CalendarCellView>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calendar_day_view, container, false);

        mWeekView = (WeekView) view.findViewById(R.id.weekView);

        mWeekView.setNumberOfVisibleDays(1);

        mWeekView.setXScrollingSpeed(0);
        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);


        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));



        int hour = displayDate.get(Calendar.HOUR_OF_DAY);
        SimpleDateFormat format = new SimpleDateFormat(" MMMM", Locale.getDefault());
        String month =  format.format(displayDate.getTime());

        Log.i(TAG, "getDisplayDate: date:" + displayDate.get(Calendar.DAY_OF_MONTH) + " Hour" + displayDate.get(Calendar.HOUR_OF_DAY));
        //display certain Date
        mWeekView.goToDate(displayDate);

        //display current hour, wired if call displayDate.get(Calendar.DAY_OF_MONTH), can't get correct result;
        Log.i(TAG, "go to Hour " + hour);
        mWeekView.goToHour(hour);


        setupDateTimeInterpreter(false);

        mTextView = (TextView) view.findViewById(R.id.back_to_month);
        mTextView.setText("<" +month);
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChangeCalendarView.changeCalendarView(CalendarContainerFragment.FragmentToShow.Month);
            }
        });


        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_sunday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_monday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_tuesday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_wednesday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_thursday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_friday));
        weekdaysRow.add((CalendarCellView) view.findViewById(R.id.row_staturday));

        Calendar tmp = (Calendar)displayDate.clone();
        //get Sunday of this week
        tmp.add(Calendar.DAY_OF_WEEK, Calendar.SUNDAY - tmp.get(Calendar.DAY_OF_WEEK));

        Calendar today = Calendar.getInstance();
        for (int i = 0; i < 7; i++) {
            CalendarCellView day = weekdaysRow.get(i);

            if (tmp.get(Calendar.DAY_OF_WEEK) == displayDate.get(Calendar.DAY_OF_WEEK)){
                setHighlightCalendarCell(day, true, tmp);
            } else {
                setHighlightCalendarCell(day, false, tmp);
            }

            day.setText(Integer.toString(tmp.get(Calendar.DAY_OF_MONTH)));
            tmp.add(Calendar.DAY_OF_WEEK, 1);
            day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CalendarCellView calendarCellView = (CalendarCellView)v;
                    Calendar today = Calendar.getInstance();
                    Calendar calendar = (Calendar)displayDate.clone();
                    calendar.add(Calendar.DAY_OF_WEEK, Calendar.SUNDAY - displayDate.get(Calendar.DAY_OF_WEEK));

                    // get clicked calendar date
                    for (int i = 0; i < 7; i++) {
                        CalendarCellView cv = weekdaysRow.get(i);
                        if (cv == calendarCellView) {
                            setHighlightCalendarCell(cv, true, calendar);
                            mWeekView.goToDate(calendar);
                        } else {
                            setHighlightCalendarCell(cv,false, calendar);
                        }
                        calendar.add(Calendar.DAY_OF_WEEK, 1);
                    }

                }
            });
        }

        return view;
    }

    // set CelendarCellView to highlighted state
    private void setHighlightCalendarCell(CalendarCellView cv,boolean highlight, Calendar calendar) {
        Calendar today = Calendar.getInstance();

        if (calendar.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH) &&
                calendar.get(Calendar.MONTH) == today.get(Calendar.MONTH)) {
            if(highlight){
                cv.setBackgroundColor(getResources().getColor(R.color.today));
                cv.setTextColor(getResources().getColor(R.color.white));
            }else{
                cv.setBackgroundColor(getResources().getColor(R.color.toolbar));
                cv.setTextColor(getResources().getColor(R.color.today));
            }
        } else {
            if (highlight){
                cv.setBackgroundColor(getResources().getColor(R.color.weekday_background));
                cv.setTextColor(getResources().getColor(R.color.white));
            } else{
                cv.setBackgroundColor(getResources().getColor(R.color.toolbar));
                cv.setTextColor(getResources().getColor(R.color.weekday_background));
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "*************onResume*************");
        Calendar startTime = Calendar.getInstance();
        startTime.add(Calendar.MONTH, -1);
        Calendar endTime = Calendar.getInstance();
        endTime.add(Calendar.MONTH, 2);
        mEvents = mEventOps.loadCalendarEvents(startTime,endTime);
//        onMonthChange(startTime.get(Calendar.YEAR), startTime.get(Calendar.MONTH));
//        mWeekView.setNumberOfVisibleDays(1);
        mWeekView.notifyDatasetChanged();
    }


    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" MMMM/dd/yyyy", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
//                return weekday.toUpperCase() + format.format(date.getTime());
                return weekday + " " + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                String string = hour < 10 ? "0" : "";
                return string + Integer.toString(hour) + ":00";
//                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        Log.i(TAG, "onMonthChange "+Integer.toString(newMonth));

        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        WeekViewEvent event;

        // Populate the week view with some events.
        // Only the events in the newMonth should be added to events, the will make sure event only add once.
        int i = 0;
        for (EventModel em : mEvents) {
            if (em.getStartDate().get(Calendar.MONTH) == newMonth) {
                event = new WeekViewEvent(i, em.getTitle(), em.getStartDate(), em.getEndDate());
                event.setColor(eventColors.get(i % 4));
                events.add(event);
            }
            i++;
        }
        return events;
    }

    private String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH)+1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(getActivity().getApplicationContext(), "Clicked " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(getActivity().getApplicationContext(), "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
    }

}
