package com.koala.timemaster;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by jq.zeng on 8/20/15.
 */
public class TagNewDialogFragment extends DialogFragment {
    private final String TAG = "TagNewDialogFragment";
    private Role mRole ;
    private TagOps tagOps;  //save tags to file
    private TagOps mCallback; //communicate with activity

    /**
     * initialize the callback function on attach
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (MoreTagsNewActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CalendarContainerFragment.ChangeCalendarView");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tagOps = new TagFileOps(getActivity().getApplicationContext());
        mRole = new Role();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.more_tags_new_dialog, null);

        final EditText mEditText = (EditText)view.findViewById(R.id.new_tag_dialog_role);
        RatingBar mRatingBar = (RatingBar) view.findViewById(R.id.new_tag_dialog_rating);
        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mRole.setWeight((int)rating);
                Log.i(TAG, "weight " + rating);
            }
        });

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.submit_string, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mRole.setTag(mEditText.getText().toString());
                        tagOps.saveTagsToFile(mCallback.addTag(mRole));
                        Toast.makeText(getActivity().getApplicationContext(), "DONE", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.cancel_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getActivity().getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
                    }
                });
        return builder.create();
    }

    public Role getRole() {
        return mRole;
    }

    public void setRole(Role role) {
        mRole = role;
    }
}
