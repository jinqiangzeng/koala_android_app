package com.koala.timemaster;

import android.content.Context;
import android.provider.Settings;

import com.example.jqzeng.mylibrary.event.EventModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by jq.zeng on 10/10/15.
 */
public class CalendarEventSerializer implements JsonSerializer<List<EventModel>> {
    @Override
    public JsonElement serialize(List<EventModel> listCalendarEvent, Type typeOfSrc, JsonSerializationContext context) {
        SimpleDateFormat format = new SimpleDateFormat(" MM/dd/yyyy hh:mm", Locale.getDefault());
        final JsonArray jsonArray = new JsonArray();
        EventModel calendarEvent;
        String title,  location, description, start, end;
        Calendar startDate, endDate;
        for (int i = 0; i < listCalendarEvent.size(); i++ ){
            JsonObject jsonObject = new JsonObject();
            calendarEvent = listCalendarEvent.get(i);
            if ((title = calendarEvent.getTitle())== null){
                title = "null";
            }
            if ((location = calendarEvent.getLocation()) == null) {
                location = "null";
            }
            if ((description = calendarEvent.getDescription()) == null) {
                description = "null";
            }
            if ((startDate = calendarEvent.getStartDate()) == null) {
                start = "null";
            } else {
                start = format.format(startDate.getTime());
            }
            if ((endDate = calendarEvent.getEndDate()) == null) {
                end = "null";
            } else {
                end = format.format(endDate.getTime());
            }

            jsonObject.addProperty("event_title",title);
            jsonObject.addProperty("event_place", location);
            jsonObject.addProperty("event_start", start);
            jsonObject.addProperty("event_end", end);
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public JsonElement serialize(List<EventModel> listCalendarEvent, Context context) {
        SimpleDateFormat format = new SimpleDateFormat(" MM/dd/yyyy hh:mm", Locale.getDefault());
        final JsonArray jsonArray = new JsonArray();
        EventModel calendarEvent;
        String title,  location, description, start, end;
        Calendar startDate, endDate;
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID); //machine ID
        for (int i = 0; i < listCalendarEvent.size(); i++ ){
            JsonObject jsonObject = new JsonObject();
            calendarEvent = listCalendarEvent.get(i);
            if ((title = calendarEvent.getTitle())== null){
                title = "null";
            }
            if ((location = calendarEvent.getLocation()) == null) {
                location = "null";
            }
            if ((description = calendarEvent.getDescription()) == null) {
                description = "null";
            }
            if ((startDate = calendarEvent.getStartDate()) == null) {
                start = "null";
            } else {
                start = format.format(startDate.getTime());
            }
            if ((endDate = calendarEvent.getEndDate()) == null) {
                end = "null";
            } else {
                end = format.format(endDate.getTime());
            }

            jsonObject.addProperty("machine_id", android_id);
            jsonObject.addProperty("event_title",title);
            jsonObject.addProperty("event_place", location);
            jsonObject.addProperty("event_start", start);
            jsonObject.addProperty("event_end", end);
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

}
