package com.koala.timemaster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.util.Charsets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jq.zeng on 8/5/15.
 */

/**
 * remember to register this class as receiver in the manifest file
 */
public class PriorityCalculator extends BroadcastReceiver {

    private static final String TAG = "PriorityCalculator";
    private List<CollectorEvent> listCollectorEvents;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.d(TAG, "Alarm is triggered");
            Toast.makeText(context, "Alarm is triggered", Toast.LENGTH_SHORT).show();
            if (listCollectorEvents == null) {
                listCollectorEvents = new ArrayList<CollectorEvent>();
            }
            loadEvents(context);
            //TODO change nice vaule according to time
            Collections.sort(listCollectorEvents,CollectorEvent.CollectorEventComparator);
            saveEvent(context);
        } catch (Exception e) {
            Toast.makeText(context, "There was an error somewhere, but we still received an alarm", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void loadEvents(Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileInputStream fis = context.getApplicationContext().openFileInput(CollectorFragment.FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                Log.d(TAG, "string builder len = "+ stringBuilder.length());
            }
            bufferedReader.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String json = stringBuilder.toString();
        Log.d(TAG, "json length = " + json.length() + ":" + json);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(new TypeToken<List<CollectorEvent>>(){}.getType(), new CollectorEventDeserializer());
        Gson gson = gsonBuilder.create();
        List<CollectorEvent> list = gson.fromJson(json, new TypeToken<List<CollectorEvent>>(){}.getType());
        if (list != null) {
            listCollectorEvents = list;
        }
    }

    private void saveEvent(Context context) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(new TypeToken<List<CollectorEvent>>(){}.getType(), new CollectorEventSerializer());
            Gson gson = gsonBuilder.create();
            String s = gson.toJson(listCollectorEvents, new TypeToken<List<CollectorEvent>>(){}.getType());
            Log.d(TAG, "writing JSON length =" + s.length() + ":" + s);
            FileOutputStream fileOutputStream = context.getApplicationContext().openFileOutput(CollectorFragment.FILE_NAME,context.getApplicationContext().MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Charsets.UTF_8.name());
            Writer jwriter = new BufferedWriter(outputStreamWriter);
            jwriter.write(s);

            jwriter.close();  //closing sequence mustn't be wrong
            outputStreamWriter.close();
            fileOutputStream.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
