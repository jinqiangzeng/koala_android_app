package com.koala.timemaster;

/**
 * Created by jq.zeng on 10/8/15.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.jqzeng.mylibrary.event.EventModel;
import com.example.jqzeng.mylibrary.event.EventsProxy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * query upload native calendar files to server in JSON format
 * remember to register the receiver in the manifest
 *
 */
public class UploadService extends BroadcastReceiver implements EventsProxy.Listener{
    private final static String FILE_NAME = "metadata.json";
    private MetaData mMetaData;
    protected Context mContext;
    final private static String TAG = "UploadService";
    final private static String SERVER_URL = "http://192.168.0.17:8080/mobilecalendar";

    @Override
    public void onReceive(Context context, Intent intent) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.getDefault());
        try{
            mContext = context;
            Log.i(TAG, "Start to upload native calendars and collector events to server");

            EventsProxy mEventsQueryHandler;
            mEventsQueryHandler = new EventsProxy(context.getContentResolver());
            mEventsQueryHandler.registerListener(this);

            mMetaData =  loadMetaData();
            Calendar start;
            Calendar now = Calendar.getInstance();
            //retrieve events from database
            if (mMetaData == null) {
                Log.d(TAG, "First time");
                start = Calendar.getInstance();
                start.add(Calendar.MONTH, -12);
            }else {
                start = mMetaData.getCalendarUploadDate();
                Log.d(TAG,"MetaData date :" + format.format(start.getTime()));
            }
            mEventsQueryHandler.get(EventModel.create(), start, now);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * load meta data from saved json file
     */
    public MetaData loadMetaData() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mContext.openFileInput(FILE_NAME)));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String json = stringBuilder.toString();
        Log.d(TAG, "loadMetaData: " + json);
        if (json.equals(""))
            return null;
        else {
            Gson gson = new Gson();
            return gson.fromJson(json, MetaData.class);
        }
    }

    /**
     * save MetaData to disk in json.
     */
    public void saveMetaData(MetaData metaData){
        try{
            Gson gson = new Gson();
            String string = gson.toJson(metaData);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(mContext.openFileOutput(FILE_NAME, mContext.MODE_PRIVATE)));
            bufferedWriter.write(string);
            bufferedWriter.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * upload native calendars to server, in json format
     */
    public boolean uploadNativeCalendarToServer(List<EventModel> events) {
        // update the metadata date when no events retrieved
        if (events == null || events.size() == 0){
            return true;
        }
        CalendarEventSerializer mCalendarEvnetSerializer = new CalendarEventSerializer();
        JsonArray jsonArray = (JsonArray) mCalendarEvnetSerializer.serialize(events, mContext);
        Gson gson = new Gson();
        Log.d(TAG, gson.toJson(jsonArray));

        Ion.with(mContext)
                .load("POST", SERVER_URL)
                .setJsonArrayBody(jsonArray)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (result != null) {
                            Log.i(TAG, result.getResult());
                        } else {
                            Log.i(TAG, "result is null");
                        }
                    }
                });
        return true;
    }

    /**
     * upload collector events to server in json format
     */
    public boolean uploadCollectorEventsToServer() {
        return true;
    }

    //Events related
    @Override
    public void onEventsRetrieved(List<EventModel> events) {
        Log.i(TAG, "retrieved " + (events==null?0:events.size()) + "events");
        //upload events to server, if events is null, it's considered uploading successfully.
        if (uploadNativeCalendarToServer(events)) {
            //set metadata's upload date to now
            if (mMetaData == null) {
                mMetaData = new MetaData();
            }
            mMetaData.setCalendarUploadDate(Calendar.getInstance());
            saveMetaData(mMetaData);
        }
    }

    @Override
    public void onEventCreated() {
        Log.i(TAG,"Calendar onEventCreated");
    }

    @Override
    public void onEventDeleted() {
        Log.i(TAG, "Calendar onEventDeleted");
    }

    @Override
    public void onEventUpdated() {
        Log.i(TAG, "Calendar onEventUpdated");
    }

}
