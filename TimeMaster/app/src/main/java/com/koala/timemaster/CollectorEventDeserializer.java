package com.koala.timemaster;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jq.zeng on 10/3/15.
 */
public class CollectorEventDeserializer implements JsonDeserializer<List<CollectorEvent>> {

    @Override
    public List<CollectorEvent> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        List<CollectorEvent> listCollectorEvent = new ArrayList<CollectorEvent>();
        String title, role, location, priority, description;
        if (json.isJsonArray()) {
            for (JsonElement e : json.getAsJsonArray()) {
                JsonObject jsonObject = e.getAsJsonObject();
                title = jsonObject.get("Title").getAsString();
                role = jsonObject.get("Role").getAsString();
                location = jsonObject.get("Location").getAsString();
                priority = jsonObject.get("Priority").getAsString();
                description = jsonObject.get("Description").getAsString();
                if (location.equals("null"))
                    location = null;
                if (description.equals("null"))
                    description = null;

                listCollectorEvent.add(new CollectorEvent(title, role, location, CollectorEvent.Priority.valueOf(priority), description));
            }
        } else if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();
            title = jsonObject.get("Title").getAsString();
            role = jsonObject.get("Role").getAsString();
            location = jsonObject.get("Location").getAsString();
            priority = jsonObject.get("Priority").getAsString();
            description = jsonObject.get("Description").getAsString();
            if (location.equals("null"))
                location = null;
            if (description.equals("null"))
                description = null;

            listCollectorEvent.add(new CollectorEvent(title, role, location, CollectorEvent.Priority.valueOf(priority), description));
        } else {
            throw new RuntimeException("Unexpected Json type: " + json.getClass());
        }

        return listCollectorEvent;
    }

}
